# frozen_string_literal: true

Rails.application.routes.draw do
  get 'fake_traffic_light/register'
  get 'fake_traffic_light/notify'
  get 'welcome/index'

  resources :emergency_vehicles do
    get 'activate', on: :member
    get 'start_emergency', on: :member
    get 'stop_emergency', on: :member
  end

  resources :fake_traffic_light do
    get 'register', on: :member
    get 'notify', on: :member
  end

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
