class CreateEmergencyVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :emergency_vehicles do |t|
      t.text :rfid
      t.text :ev_type
      t.text :status
      t.text :uuid
      t.text :obs
      t.numeric :lat
      t.numeric :lon
      t.text :priority

      t.timestamps
    end
    add_index :emergency_vehicles, :rfid, unique: true
  end
end
