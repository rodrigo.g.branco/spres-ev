require 'test_helper'

class FakeTrafficLightControllerTest < ActionDispatch::IntegrationTest
  test "should get register" do
    get fake_traffic_light_register_url
    assert_response :success
  end

end
