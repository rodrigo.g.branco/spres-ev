# frozen_string_literal: true

require 'rest-client'
require 'json'

# Fake Traffic Light Controller
class FakeTrafficLightController < ApplicationController
  # Used to allow SUMO to use - this class should not be used in production!
  protect_from_forgery with: :null_session

  def register
    log("Teste!xxx #{params}", __LINE__)

    interscity_instance = FakeTrafficLightInterscity.new(
      description: 'fakeTrafficLight',
      status: 1,
      lat: 0,
      lon: 0,
      traffic_light_with_rfid_reader: capability
    )

    uuid = interscity_instance.search_by_description(capability)

    log("UUID: #{uuid}", __LINE__)

    uuid = interscity_instance.create_resource if uuid.nil? || uuid.empty?

    subscription_id = check_subscription(uuid)

    if subscription_id.nil?
      create_subscription(url_param, uuid)
    else
      update_subscription(subscription_id, url_param)
    end

    render json: uuid
  end

  def notify
    log("Teste notify! #{params}", __LINE__)

    values = notify_params

    interscity_instance = EmergencyVehicleInterscity.new(
      uuid: values[:uuid]
    )

    interscity_instance = EmergencyVehicleInterscity.new(
      interscity_instance.search_by_uuid
    )

    log("tls_id: #{values[:tls_id]}", __LINE__)

    tl_command = []
    values[:tls_id].each do |tl|
      tl_command << {
        uuid: values[:uuid],
        capabilities: {
          traffic_light_with_rfid_reader: {
            order: values[:command],
            target: values[:ev],
            tl: tl,
            tag: values[:tag]
          }
        }
      }
    end

    interscity_instance.notify_traffic_lights(tl_command)

    render json: 'commands sent'
  end

  private

  def notify_params
    params.require(:fake_traffic_light)
          .permit(:uuid, { :tls_id => [] }, :ev, :command, :tag)
  end

  def url_param
    webhook_endpoint = params.require(:fake_traffic_light).permit(:url)
    webhook_endpoint[:url]
  end

  def capability
    'traffic_light_with_rfid_reader'
  end

  def update_subscription(subscription_id, webhookEndpoint)
    log("webhookEndpoint for update ID #{subscription_id}: #{webhookEndpoint}", __LINE__)
    begin
      response = RestClient.put(
        "#{ENV['ADAPTOR_HOST']}/subscriptions/#{subscription_id}",
        'subscription': {
          'url': webhookEndpoint
        }
      )
      return JSON.parse(response.body)['subscription']
    rescue Exception => e
      log("Could not update subscription:
          #{e.class} - #{e.message}", __LINE__)
      return false
    end
  end

  def create_subscription(webhookEndpoint, uuid)
    log("webhookEndpoint for creation UUID #{uuid}: #{webhookEndpoint}", __LINE__)
    begin
      response = RestClient.post(
        "#{ENV['ADAPTOR_HOST']}/subscriptions",
        'subscription': {
          'uuid': uuid,
          'capabilities': [capability],
          'url': webhookEndpoint
        }
      )
      return JSON.parse(response.body)['subscription']
    rescue Exception => e
      log("Could not create subscription:
          #{e.class} - #{e.message}", __LINE__)
      return false
    end
  end

  def check_subscription(uuid)
    begin
      resp = RestClient.get(
        "#{ENV['ADAPTOR_HOST']}/subscriptions",
        params: {
          'uuid': uuid
        }
      )
      log("Check Sub: #{resp}", __LINE__)
      subs = JSON.parse(resp.body)

      return subs['subscriptions'].first['id'] unless
             subs.nil? || subs.empty? ||
             subs['subscriptions'].nil? || subs['subscriptions'].empty? ||
             subs['subscriptions'].first.nil? ||
             subs['subscriptions'].first.empty? ||
             subs['subscriptions'].first['id'].nil?

      return nil
    rescue StandardError => e
      log("Could not check subscription:
          #{e.class} - #{e.message}", __LINE__)
      return nil
    end
  end

  def log(msg, line)
    Rails.logger.tagged('SPreS-EV') {
      Rails.logger.debug(__FILE__ + "[L#{line}]:: #{msg}")
    }
  end
end
