# frozen_string_literal: true

# Welcome Controller. Where everything starts
class WelcomeController < ApplicationController
  def index
    @allow_proceed = register_capabilities('ev_monitoring', cap_ev_desc) &&
                     register_capabilities('traffic_light_with_rfid_reader',
                                           cap_tl_desc)
  end

  private

  def register_capabilities(cap_name, cap_data)
    puts "#{capabilities_path}#{cap_name}"
    begin
      RestClient.get(
        "#{capabilities_path}#{cap_name}"
      )
      puts "Capability #{cap_name} is already created, skipping..."
      # puts "Updating capability #{cap_name}"
      # update = RestClient.put(
      #    "#{capabilities_path}#{cap_name}",
      #    capability_data
      # )
      return true
    rescue Exception => e
      if e.is_a? RestClient::ExceptionWithResponse
        if e.http_code == 404
          puts "Capability #{cap_name} not found, trying to create it..."
          begin
            RestClient.post(
              capabilities_path,
              cap_data
            )
            puts "Capability #{cap_name} created"
            return true
          rescue RestClient::Exception => e
            puts "Could not register capability: #{e.response}"
            return false
          end
        else
          puts "Unknown error 1:  #{e.response}"
          return false
        end
      else
        puts "Unknown error 2: #{e}"
        return false
      end
    end
  end

  def capabilities_path
    "#{ENV['CATALOGUER_HOST']}/capabilities/"
  end

  def cap_ev_desc
    {
      name: 'ev_monitoring',
      description: 'Provides data about Emergency Vehicles, including their RFID and GPS information',
      capability_type: 'sensor'
    }
  end

  def cap_tl_desc
    {
      name: 'traffic_light_with_rfid_reader',
      description: 'Capability of Traffic Light whose has RFId reader and can receive commands',
      capability_type: 'actuator'
    }
  end
end
