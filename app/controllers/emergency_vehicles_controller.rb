# frozen_string_literal: true

# Emergency Vehicle Controller
class EmergencyVehiclesController < ApplicationController
  # Used to allow SUMO to use - should not be used in production!
  protect_from_forgery with: :null_session

  def index
    @emergency_vehicles = EmergencyVehicle.all
  end

  def show
    @emergency_vehicle = EmergencyVehicle.find(params[:id])
  end

  def new
    @emergency_vehicle = EmergencyVehicle.new
  end

  def edit
    @emergency_vehicle = EmergencyVehicle.find(params[:id])
  end

  def create
    @emergency_vehicle = EmergencyVehicle.new(ev_params)
    @emergency_vehicle.status = 'created'

    begin
      if @emergency_vehicle.save
        register_interscity

        @emergency_vehicle.update_columns(uuid: @emergency_vehicle.uuid,
                                          status: @emergency_vehicle.status)

        redirect_to @emergency_vehicle
      else
        render 'new'
      end
    rescue ActiveRecord::RecordNotUnique
      redirect_to emergency_vehicles_path,
                  flash: { error: "That RFId(#{@emergency_vehicle.rfid})
                                    is already registered" }
    end
  end

  def update
    @emergency_vehicle = EmergencyVehicle.find(params[:id])

    if @emergency_vehicle.update(ev_params)
      redirect_to @emergency_vehicle
    else
      render 'edit'
    end
  end

  def destroy
    @emergency_vehicle = EmergencyVehicle.find(params[:id])
    @emergency_vehicle.destroy

    redirect_to emergency_vehicles_path
  end

  def activate
    @emergency_vehicle = EmergencyVehicle.find(params[:id])

    if @emergency_vehicle.status == 'created' ||
       @emergency_vehicle.status == 'pending'
      register_interscity

      if @emergency_vehicle.uuid.nil? || @emergency_vehicle.uuid.empty?
        redirect_to emergency_vehicles_path,
                    flash: { error: 'This EV could not be
                                     activated in InterSCity' }
      else
        @emergency_vehicle.update_columns(uuid: @emergency_vehicle.uuid,
                                          status: @emergency_vehicle.status)
        redirect_to emergency_vehicles_path
      end
    else
      redirect_to emergency_vehicles_path,
                  flash: { error: 'Only EVs with created or pending status
                                   can be switched to active' }
    end
  end

  def start_emergency
    @emergency_vehicle = EmergencyVehicle.find(params[:id])

    if @emergency_vehicle.status == 'active'
      notify_traffic_lights(@emergency_vehicle, 'authorize')

      @emergency_vehicle.update_columns(status: 'emergency')

      log('Starting emergency...', __LINE__)

      redirect_to emergency_vehicles_path
    else
      redirect_to emergency_vehicles_path,
                  flash: { error: 'Only EVs with active status
                                   can be switched to emergency' }
    end
  end

  def stop_emergency
    @emergency_vehicle = EmergencyVehicle.find(params[:id])

    if @emergency_vehicle.status == 'emergency'
      notify_traffic_lights(@emergency_vehicle, 'deny')

      @emergency_vehicle.update_columns(status: 'active')

      log('Stoping emergency...', __LINE__)

      redirect_to emergency_vehicles_path
    else
      redirect_to emergency_vehicles_path,
                  flash: { error: 'Only EVs with emergency status
                                   can stop preemption' }
    end
  end

  private

  def ev_params
    params.require(:emergency_vehicle).permit(:rfid, :obs, :ev_type, :status)
  end

  def log(msg, line)
    Rails.logger.tagged('SPreS-EV') {
      Rails.logger.debug(__FILE__ + "[L#{line}]:: #{msg}")
    }
  end

  def notify_traffic_lights(ev, command)
    interscity_instance = EmergencyVehicleInterscity.new(
      uuid: ev.uuid
    )

    interscity_instance = EmergencyVehicleInterscity.new(
      interscity_instance.search_by_uuid
    )

    log("got: #{interscity_instance}", __LINE__)

    tl_in_radius = interscity_instance.discovery_resources_by_radius(
      'traffic_light_with_rfid_reader',
      3000
    )

    tl_command = []
    tl_in_radius.each do |tl|
      tl_command << {
        uuid: tl['uuid'],
        capabilities: {
          traffic_light_with_rfid_reader: {
            order: command,
            # target: tl['description']
            target: ev.uuid
          }
        }
      }
    end

    log("got tls: #{tl_in_radius}", __LINE__)
    log("tl_command: #{tl_command}", __LINE__)

    # Most important part: notify Traffic Lights to stop preemption!!!!!
    log('Here we must notify important traffic
         lights to stop preemption!', __LINE__)

    interscity_instance.notify_traffic_lights(tl_command)

    interscity_instance.send_data_with_data('ev_monitoring', command)
  end

  def register_interscity
    create_capability

    interscity_instance = EmergencyVehicleInterscity.new(
      description: @emergency_vehicle.rfid,
      status: 1,
      # change lat,lon to zero later
      lat: -23.571297,
      lon: -46.644181,
      ev_monitoring: 'ev_monitoring'
    )

    uuid = interscity_instance.search_by_description('ev_monitoring')

    log("UUID: #{uuid}", __LINE__)

    if uuid.nil? || uuid.empty?
      interscity_instance.create_resource

      if interscity_instance.uuid.nil? || interscity_instance.uuid.empty?
        @emergency_vehicle.status = 'pending'
      else
        @emergency_vehicle.status = 'active'
        @emergency_vehicle.uuid = interscity_instance.uuid
      end
    else
      @emergency_vehicle.status = 'active'
      @emergency_vehicle.uuid = uuid
    end
  end

  # Checking if specific capability is already created
  def create_capability
    capabilities_path = "#{ENV['CATALOGUER_HOST']}/capabilities/"
    capability_name = 'ev_monitoring'

    capability_data = {
      name: capability_name,
      description: 'Provides data about Emergency Vehicles, including their RFID and GPS information',
      capability_type: 'sensor'
    }

    begin
        RestClient.get(
          capabilities_path + capability_name
        )
        log("Updating capability #{capability_name}", __LINE__)
        RestClient.put(
          capabilities_path + capability_name,
          capability_data
        )
      rescue Exception => e
        if e.http_code == 404
          log("Capability #{capability_name} not found,
              trying to create it...", __LINE__)
          begin
            RestClient.post(
              capabilities_path,
              capability_data
            )
            log("Capability #{capability_name} created", __LINE__)
          rescue Exception => e
            log("Could not register capability: #{e}", __LINE__)
          end
        else
          log("Unknown error: #{e}", __LINE__)
        end
      end
  end
end
