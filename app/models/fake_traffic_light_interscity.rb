# frozen_string_literal: true

# Fake Traffic Light in InterSCity for use with SUMO Simulator
class FakeTrafficLightInterscity < BasicResourceInterscity
  attr_accessor :traffic_light_with_rfid_reader

  def initialize(params = {})
    super
    self.capabilities = [
      'traffic_light_with_rfid_reader'
    ]
  end
end
