# frozen_string_literal: true

# Enumaration of status types allowed for EVs
class EmergencyVehicle < ApplicationRecord
  validates :status, inclusion: {
    in: %w[created pending active emergency]
  }
end
