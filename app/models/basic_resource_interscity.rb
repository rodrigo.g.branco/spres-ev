# frozen_string_literal: true

require 'date'
require 'json'
require 'rest-client'

class BasicResourceInterscity 
  attr_accessor :id, :uuid
  attr_accessor :lat, :lon, :description, :capabilities, :status

  def initialize(params={})
    params.each do |key, value|
      instance_variable_set("@#{key}", value)
    end
  end

  def data
    {
      lat: self.lat,
      lon: self.lon,
      description: self.description,
      capabilities: self.capabilities,
      status: self.status
    }
  end

  def search_by_uuid
    begin
      log("#{ENV['CATALOGUER_HOST']}/resources/#{self.uuid}", __LINE__)
      response = RestClient.get(
        "#{ENV['CATALOGUER_HOST']}/resources/#{self.uuid}"
      )
      resp = JSON.parse(response.body)['data']
      log("data: #{resp}", __LINE__)
      return resp
    rescue => e
      log("Could not get registered resource by name !:
           #{e.class} - #{e.message}", __LINE__)
    end
  end

  def search_by_description(capability_name)
    begin
      response = RestClient.get(
        "#{ENV['CATALOGUER_HOST']}/resources/search",
        {
          params: {
            capability: capability_name,
            description: self.description
          }
        }
      )
      resp = JSON.parse(response.body)['resources']

      if resp.nil? || resp.empty? || resp.first.nil? ||
         resp.first.empty? || resp.first['uuid'].nil? ||
         resp.first['uuid'].empty?
        return nil
      else
        resp.first['uuid']
      end
    rescue Exception => e
      log("Could not get registered resource by name !:
           #{e.class} - #{e.message}", __LINE__)
      return nil
    end
  end

  def notify_traffic_lights(command)
    begin
      response = RestClient.post(
        "#{ENV['ACTUATOR_HOST']}/commands",
        {
          data: command
        }
      )
      success = JSON.parse(response.body)['success']
      failure = JSON.parse(response.body)['failure']
      log("success: #{success}", __LINE__)
      log("failure: #{failure}", __LINE__)
    rescue Exception => e
      log("Could not send command:
          #{e.class} - #{e.message}", __LINE__)
    end
  end

  def discovery_resources_by_radius(capability_name, radius)
    begin
      response = RestClient.get(
        "#{ENV['DISCOVERY_HOST']}/resources",
        {
          params: {
            lat: self.lat,
            lon: self.lon,
            radius: radius,
            capability: capability_name
          }
        }
      )
      resp = JSON.parse(response.body)['resources']
      log("resources: #{resp}", __LINE__)
      return resp
    rescue => e
      log("Could not get resources by radius!:
           #{e.class} - #{e.message}", __LINE__)
    end
  end

  def get_all_registered_resources(capability_name)
    begin
      response = RestClient.get(
        "#{ENV['CATALOGUER_HOST']}/resources/search",
        {
          params: {
            capability: capability_name
          }
        }
      )
      resp = JSON.parse(response.body)['resources']
      return resp
    rescue Exception => e
      log("Could not get all registered Resources!
          #{e.class} - #{e.message}", __LINE__)
      return []
    end
  end

  def create_resource
    begin
      response = RestClient.post(
        "#{ENV['CATALOGUER_HOST']}/resources",
        {
          data: self.data
        }
      )
      self.uuid = JSON.parse(response.body)['data']['uuid']
    rescue Exception => e
      log("Could not register resource:
          #{e.class} - #{e.message}", __LINE__)
      return false
    end
  end

  def update_resource
    begin
      RestClient.put(
        "#{ENV['CATALOGUER_HOST']}/resources/#{self.uuid}",
        {
          data: self.data
        }
      )
      log("Resource #{self.description}
          with #{self.uuid} updated!", __LINE__)
      return true
    rescue Exception => e
      log("Could not update capability: #{e.class} - #{e.message}",__LINE__)
      return false
    end
  end

  def send_data_with_data(capability_name, value)
    timestamp = DateTime.now.to_s
    capability = capability_name.to_s

    data_json = {}
    data_json[capability] = [{ location: { lat: lat, lon: lon }, value: value, timestamp: timestamp }]

    begin
      RestClient.post(
        "#{ENV['ADAPTOR_HOST']}/resources/#{self.uuid}/data",
        {
          data: data_json
        }
      )
      log("Resource #{self.description} with #{self.uuid}
           has had its data updated!", __LINE__)
    rescue Exception => e
      log("Could not send data of resource #{self.description} 
           with #{self.uuid}: #{e.class} - #{e.message}", __LINE__)
    end
  end

  def send_data(capability_name)
    value = self.send(capability_name)
    send_data_with_data(capability_name, value)
  end

  def get_last_data(capability_name)
    begin
      response = RestClient.post(
        "#{ENV['COLLECTOR_HOST']}/resources/#{self.uuid}/data/last",
        {
          data: ''
        }
      )
      ret = JSON.parse(response.body)['resources'].first['capabilities']
      return [] unless !ret.nil? && !ret.empty?
      ret = ret[capability_name]

      return [] unless !ret.nil? && !ret.empty?
      return ret
    rescue Exception => e
      log("Could not get data of resource #{self.description}
           with #{self.uuid}: #{e.class} - #{e.message}", __LINE__)
      return ''
    end
  end

  def log(msg, line)
    Rails.logger.tagged('SPreS-EV') {
      Rails.logger.debug(__FILE__ + "[L#{line}]:: #{msg}")
    }
  end
end
