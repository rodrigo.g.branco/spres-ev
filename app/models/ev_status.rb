# frozen_string_literal: true

class EVStatus
  EV_STATUS = %w[created pending active emergency inactive].freeze
end
