# frozen_string_literal: true

# Emergency Vehicle Representation in InterSCity
class EmergencyVehicleInterscity < BasicResourceInterscity
  attr_accessor :ev_monitoring

  def initialize(params = {})
    super
    self.capabilities = [
      'ev_monitoring'
    ]
  end
end
